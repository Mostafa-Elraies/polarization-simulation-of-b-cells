// this model the B cell polarization in the existance of actin cage and tethering the nucleus to the centrosome via actin filaments.

set simul system
{
    time_step = 0.04
    viscosity = 0.1
    steric = 1, 100, 0
}

set space cell
{
    shape = hemisphere
    display = (color= 1 1 1 0.4; width =4)
}

set space centrosome_confiner
{
    shape = hemisphere
    display = (visible = 1; width = 2; color =  1 0 0 0.2)
}
new centrosome_confiner
{
    radius = 7
    fraction = 0.2
    interpolation_distance = 1
}
//--------------------------------------------------
//--------------------------------------------------
set space actin_confiner
{
    shape = cylinderY
}

new actin_confiner
{
    bottom = -6
    top = -4.2
    radius = 3   
}




set fiber actin
{
    lattice = 1, 0.02    

    confine = inside, 100
    steric = 1, 0.0035
    rigidity = 0.075
    segmentation = 0.1

    activity        = treadmill
    growing_speed   =  0.0, 0.0  
    shrinking_speed = -0.0, -0.0            
    growing_force   = 3.0, 3.0      
    binding_key = 2
}
new 750 fiber actin
{
    length = 2, exponential
    end_state = white, red
    placement = inside, actin_confiner
}

set hand binder
{
    binding_rate = 50
    binding_range = 0.5
    unbinding_rate = 0.1
    unbinding_force = 6
    binding_key = 2
    display = ( color=red; size=7; width=7; )
}

set single crosslinker
{
    hand = binder
    stiffness = 10
    activity = fixed
} 
new 0 crosslinker{}


set hand binder_cross
{
    binding_rate = 10
    binding_range = 0.1
    unbinding_rate = 0.1
    unbinding_force = 6

    display = ( color=blue; size=4; )
}

set couple cross_linker
{
    hand1 = binder_cross
    hand2 = binder_cross
    diffusion = 1
    length = 0.1
    stiffness = 100
} 
new 200 cross_linker   


//--------------------------------------------------
//--------------------------------------------------

set solid envelope
{
    confine = all_inside, 100, cell
    steric = 1, 100
    display = ( style=3 )
    
}

new cell
{
    radius = 7
    fraction = 1
}



//--------------------------------------------------


//--------------------------------------------------

set fiber microtubule
{
    lattice = 1, 0.02    

    confine = inside, 100, cell
    steric = 1, 0.012
    rigidity = 20
    segmentation = 0.2

    activity = classic
    growing_speed = 0.3
    shrinking_speed  = -1
    catastrophe_rate = 0.058, 0.15
    max_chewing_speed = -0.5
    rescue_rate      = 0
    growing_force    = 5
    min_length       = 0.5

    display = ( color=white; speckles=4,2,1; width=1.5; plus_end=12; )
}
set solid core
{
    display = ( style=3 )
    confine = all_inside, 100, centrosome_confiner
}


set aster centrosome
{
    stiffness = 500, 500
    nucleate = 1, microtubule, ( plus_end=grow;length=0.2; )
}

new 1 centrosome
{
    solid  = core
    radius = 0.5

    fibers = 150, microtubule, ( plus_end=grow; length = 1, exponential; )
    placement = inside, centrosome_confiner
    sphere1 = 1 0 0, 0.5, 40 crosslinker}

new 1 envelope
{
    sphere1 = sphere 1.0, 5, 350 crosslinker
    
    }
    



run 5000 system
{
    nb_frames=500
}

//--------------------------------------------------


//--------------------------------------------------
//--------------------------------------------------





//--------------------------------------------------
//--------------------------------------------------





set space synapse_confiner
{
    shape = cylinderY
}

new synapse_confiner 
{
    bottom = 5.6
    top = 5.601
    radius = 7    
}

//--------------------------------------------------



//--------------------------------------------------

set hand sliding_dynein
{
    binding_rate = 5
    binding_range = 0.15
    unbinding_rate = 0.05
    unbinding_force = 4
    
    activity = walk
    step_size = 0.02
    footprint = 2
    site_shift = 0.015
    unloaded_speed = -0.5
    stall_force = 4

    bind_also_end = 1
    hold_growing_ends = 0.999

    display = ( color=light_blue; size=7; width=7; )
}

set hand capture_shrinkage_dynein
{
    binding_rate = 5
    binding_range = 0.15
    unbinding_rate = 0.0001
    unbinding_force = 4
    stall_force = 4
    bind_only_end = plus_end
    bind_end_range = 0.1

    hold_growing_end = 1
    hold_shrinking_end = 1

    activity = chew
    chewing_speed = -0.5
    display = ( color = red )
}

set single graftedD
{
    hand = sliding_dynein
    stiffness = 100
    activity = fixed
}

set single graftedC
{
    hand = capture_shrinkage_dynein
    stiffness = 100
    activity = fixed
}


set bead membrane_binder 
{
    confine = surface, 100, synapse_confiner
    display = ( style=1 )
    drag = 1
}



change space cell
{
    fraction = 0.9
    interpolation_distance = 1
}

run 100 system
{
    nb_frames=10
}

change actin 
{
    growing_speed   =  0.0, 0.0  
    shrinking_speed = -0.009, -0.009   
}

change space centrosome_confiner
{
    display=(visible=0)
}

change solid core
{
    confine = all_inside, 100, cell
}

new 0 graftedD{
    placement =surface, synapse_confiner
}

new 0 graftedC{
    placement =surface, synapse_confiner
}


new 120 membrane_binder
{
    radius = 0.01
    attach = graftedD
    position = discXZ 4 0.1 at 0 5.6 0
    placement = anywhere
}

new 0 membrane_binder
{
    radius = 0.01
    attach = graftedC
    position = discXZ 4 0.1 at 0 5.6 0
    placement = anywhere
}


run 11250 system
{
    nb_frames=1125
}

